import { Component } from "@angular/core";
import { ViewController } from "ionic-angular";

@Component({
  // selector: 'page-modal',
  template: `

    <div class="mainDiv">
    <!-- <ion-buttons end (click)="dismiss()"> -->
            <!-- <button ion-button icon-only (click)="dismiss()"> -->
                <!-- <ion-icon name="close-circle" style=" font-size: 2.2em;
                color: white;"></ion-icon> -->
            <!-- </button> -->
        <!-- </ion-buttons> -->
        <ion-buttons end>
            <button ion-button icon-only (click)="dismiss()">
                <ion-icon name="close-circle"></ion-icon>
            </button>
        </ion-buttons>
      <div class="secondDiv">
        <ion-grid>
          <ion-grid>
            <ion-row no-padding>

              <ion-col width-50>
                <div class="design">
                  <span class="cont">
                    <p>ARM</p>
                  </span>
                  <div
                    style="height: 60px; width: 70px; margin: auto; text-align: center;"
                  >
                    <img src="assets/imgs/lock.png" />
                  </div>
                </div>
              </ion-col>
              <ion-col width-50>
                <div class="design">
                  <span class="cont">
                    <p>DISARM</p>
                  </span>
                  <div
                    style="height: 60px; width: 70px; margin: auto; text-align: center;"
                  >
                    <img src="assets/imgs/unlock.png" />
                  </div>
                </div>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col width-50>
                <div class="design">
                  <div
                    style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px"
                  >
                    <img src="assets/imgs/play.png" />
                  </div>
                  <span class="cont">
                    <p>RESUME CAR</p>
                  </span>
                </div>
              </ion-col>
              <ion-col width-50>
                <div class="design">
                  <div
                    style="height: 60px; width: 70px; margin: auto; text-align: center; padding-top: 38px"
                  >
                    <img src="assets/imgs/stop.png" />
                  </div>
                  <span class="cont">
                    <p>STOP CAR</p>
                  </span>
                </div>
              </ion-col>
            </ion-row>
          </ion-grid>
          <div class="final">
            <img src="assets/imgs/support.png" class="manual" />
            <p>LISTEN</p>
            <!-- <span class="cont">
      <p>STOP CAR</p>
  </span>  -->
          </div>
        </ion-grid>
        <div style="padding: 5px;">
          <button ion-button (click)="dismiss()" color="gpsc" block>
            Submit
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .mainDiv {
        padding: 80px 10px;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.7);
      }

      .secondDiv {
        padding-top: 20px;
        border-radius: 18px;
        border: 2px solid black;
        background: white;
      }
      .design {
    height: 150px;
    width: 100%;
    background-color: #d6d8d7;
}

.ionCol {
    background-color: blue($color: #000000);
}
.final {
    top: 38%;
    left: calc(50% - 65px);
    position: fixed;
    background: #d6d8d7;
    width: 130px;
    height: 130px;
    border-radius: 50%;
    text-align: center;
    padding-top: 40px;
    border: 5px solid white;
}
.cont {
    text-align: center;
    vertical-align: middle;
    line-height: 90px;
}
p {
    margin: auto;
    height: 54px
}
img {
    max-width: 50%;
    border: 0;
    margin-top: 10px;
}
.manual {
    margin: auto;
    max-width: 35%;
    margin-top: -12px;
}
    `
  ]
})
export class ImmobilizeModelPage {
  min_time: any = "10";
  constructor(
    // private navParams: NavParams,
    private viewCtrl: ViewController
  ) {}

  dismiss() {
    console.log("Inside the function");
    this.viewCtrl.dismiss();
  }
}
